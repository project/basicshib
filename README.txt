CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

This module provides Basic Shibboleth Authentication. Shibboleth is a 
single-sign-on (SSO) solution part of the InCommon Federation. The module
delegates several more complex tasks to plugins. The plugins used in 
this module are:
    * UserProvider- loads users.
    * AuthFilter- applies additional authentication rules.
    * Grouper- assigns Role permissions based on user's Grouper group.

For a full description of the module, visit the project page:
https://www.drupal.org/project/basicshib

For a description of Shibboleth SSO, visit the InCommon site:
https://www.incommon.org/software/shibboleth/

For a description of Grouper software, visit the InCommon site:
https://incommon.org/software/grouper/

REQUIREMENTS
------------

* This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/project/basicshib/releases/3.0-dev for more details.

* A PHPUnit version of 6.5 may be required for unit testing compatibility.

CONFIGURATION
------------

* Enable Basic Shibboleth Authentication in Home >> Extend >> Authentication.

* To enable the Grouper Plugin, go to Configuration >> BasicShib settings:

    - In the PLUGIN_ENABLED box, click on Enable Grouper and Save configuration
    to enable the plugin.

    - The Grouper Settings link now visible in the PLUGIN_ENABLED box will direct
    administrators to a form. Here, site administrators will assign site-specific
    roles to user-inputted Grouper paths.

    - Two roles, authenticated user and anonymous, are omitted from this form. Grouper
    paths are ':' delimited strings. Multiple paths should be separated by a semicolon.

TROUBLESHOOTING
------------

* The Grouper plugin assumes an HTTP_ISMEMBEROF user attribute is being released by the SP. 
Should any errors arise, ensure that your SP is configured properly.
