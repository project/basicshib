<?php

namespace Drupal\basicshib\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class AuthenticationFilter.
 *
 * @package Drupal\basicshib\Annotation
 *
 * @Annotation
 */
class BasicShibAuthFilter extends Plugin {
  /**
   * Machine name of the plugin.
   *
   * @var string
   */
  public string $id;

  /**
   * Human-readable name of the plugin.
   *
   * @var string
   */
  public string $name;

}
