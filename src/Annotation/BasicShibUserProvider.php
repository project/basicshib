<?php

namespace Drupal\basicshib\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class BasicShibUserProvider.
 *
 * @package Drupal\basicshib\Annotation
 *
 * @Annotation
 */
class BasicShibUserProvider extends Plugin {
  /**
   * Machine name of the plugin.
   *
   * @var string
   */
  public string $id;

  /**
   * Human-readable name of the plugin.
   *
   * @var string
   */
  public string $name;

}
