<?php

namespace Drupal\basicshib;

use Drupal\basicshib\Exception\AttributeException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\ServerBag;

/**
 *
 */
class AttributeMapper implements AttributeMapperInterface {
  /**
   * @var ImmutableConfig
   */
  private ImmutableConfig $configuration;

  /**
   * @var ServerBag
   */
  private ServerBag $server;

  /**
   * @var array
   */
  private array $attribute_map;

  /**
   * AttributeMapper constructor.
   *
   * @param ConfigFactoryInterface $config_factory
   * @param RequestStack $request_stack
   *
   * @throws AttributeException
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack) {

    $this->configuration = $config_factory
      ->get('basicshib.settings');

    $this->server = $request_stack
      ->getCurrentRequest()
      ->server;

    $this->attribute_map = $this->getAttributeMap();
  }

  /**
   * Get an attribute's value.
   *
   * @param $id
   *   The id of the attribute to fetch.  An exception is thrown if no mapping
   *   exists for the provided id.
   *
   * @param bool $empty_allowed
   *   Whether to allow empty attributes. When false, an exception is thrown if
   *   the attribute is not set.
   *
   * @return string
   *   The value of the attribute
   *
   * @throws AttributeException
   *
   * @todo Remove $empty_allowed and check this somewhere else.
   */
  public function getAttribute($id, $empty_allowed = FALSE): ?string {
    if (isset($this->attribute_map[$id])) {
      $def = $this->attribute_map[$id];
      $value = $this->server->get($def['name']);
      if (!$value && !$empty_allowed) {
        throw new AttributeException(
                      sprintf(
                          'Attribute is not set: \'%s\' (mapped to \'%s\')',
                          $def['name'], $id
                      ),
                      AttributeException::NOT_SET
        );
      }
      return $value;
    }
    throw new AttributeException(
      sprintf(
        'Key attribute is not mapped: \'%s\'', $id
      ),
      AttributeException::NOT_MAPPED
    );
  }

  /**
   * @return array
   *
   * @throws AttributeException
   */
  private function getAttributeMap(): array {
    $attribute_map = [];

    $config = $this->configuration
      ->get('attribute_map');

    foreach ($config['key'] as $id => $name) {
      $attribute_map[$id] = [
        'id' => $id,
        'name' => $name,
        'key' => TRUE,
      ];
    }

    foreach ($config['optional'] as $def) {
      if (isset($attribute_map[$def['id']])) {
        throw new AttributeException(
                    sprintf(
                        'Attribute with id \'%s\' is already defined',
                        $def['id']
                    ), AttributeException::DUPLICATE_ID
        );
      }
      $attribute_map[$def['id']] = [
        'id' => $def['id'],
        'name' => $def['name'],
        'key' => FALSE,
      ];
    }

    return $attribute_map;
  }

}
