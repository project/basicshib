<?php

namespace Drupal\basicshib;

use Drupal\basicshib\Exception\AttributeException;
use Drupal\basicshib\Exception\AuthenticationException;
use Drupal\basicshib\Plugin\AuthFilterPluginInterface;
use Drupal\basicshib\Plugin\BasicShibPluginManager;
use Drupal\basicshib\Plugin\UserProviderPluginInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\mymodule\PathProcessor\BasicShibPathProcessor;
 use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;
use Drupal\Core\Routing\RedirectDestination;

/**
 *
 */
class AuthenticationHandler implements AuthenticationHandlerInterface {
  /**
   * Instance variable attribute mapper.
   *
   * @var AttributeMapperInterface
   */
  private AttributeMapperInterface $attribute_mapper;

  /**
   * Instance variable request stack.
   *
   * @var RequestStack
   */
  private Request $request;

  /**
   * Instance variable session tracker.
   *
   * @var SessionTracker
   */
  private SessionTracker $session_tracker;

  /**
   * Instance variable user provider.
   *
   * @var UserProviderPluginInterface
   */
  private $user_provider;

  /**
   * Instance variable auth filter .
   *
   * @var AuthFilterPluginInterface[]
   */
  private array $auth_filters = [];

  /**
   * Instance variable handlers.
   *
   * @var array
   */
  private $handlers = [];

  /**
   * Instance variable path validator.
   *
   * @var PathValidatorInterface
   */
  private PathValidatorInterface $path_validator;

  /**
   * Instance variable redirect destination.
   *
   * @var RedirectDestination
   */
  private RedirectDestination $redirect_destination;

  /**
   * Instance variable path validator.
   *
   * @var CurrentPathStack
   */
  private CurrentPathStack $current_path;

  /**
   * @var \Psr\Log\LoggerInterface
   */

  /**
   * @param ConfigFactoryInterface $config_factory
   * @param RequestStack $request_stack
   * @param AttributeMapperInterface $attribute_mapper
   * @param BasicShibPluginManager $user_provider_plugin_manager
   * @param BasicShibPluginManager $auth_filter_plugin_manager
   * @param PathValidatorInterface $path_validator
   * @param RedirectDestination $redirect_destination
   * @param CurrentPathStack $current_path
   *
   * @throws PluginException
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RequestStack $request_stack,
    AttributeMapperInterface $attribute_mapper,
    BasicShibPluginManager $user_provider_plugin_manager,
    BasicShibPluginManager $auth_filter_plugin_manager,
    PathValidatorInterface $path_validator,
    RedirectDestination $redirect_destination,
    CurrentPathStack $current_path
  ) {
    $this->session_tracker = new SessionTracker(
        $request_stack->getCurrentRequest()->getSession());
    $plugins = $config_factory
      ->get('basicshib.settings')
      ->get('plugins');
    $this->attribute_mapper = $attribute_mapper;
    $this->user_provider = $user_provider_plugin_manager
      ->createInstance($plugins['user_provider']);
    foreach ($plugins['auth_filter'] as $name) {
      $this->auth_filters[$name] = $auth_filter_plugin_manager
        ->createInstance($name);
    }

    $this->handlers = $config_factory
      ->get('basicshib.settings')
      ->get('handlers');
    $this->path_validator = $path_validator;
    $this->redirect_destination = $redirect_destination;
  }

  /**
   * {@inheritDoc}
   * @throws AttributeException
   */
  public function authenticate() {
    // Get key attributes.
    $key_attributes = $this->getKeyAttributes();
    // Load the user account.
    $account = $this->user_provider
      ->loadUserByName($key_attributes['name']);
    if ($account) {
      $this->assertExistingUserLoginAllowed($account);
    }
    else {
      $this->assertUserCreationAllowed($key_attributes['name']);
      $account = $this->user_provider
        ->createUser(
          $key_attributes['name'],
          $key_attributes['mail']
        );
    }

    $this->saveAccount($account, $key_attributes);
    $this->session_tracker->set($key_attributes['session_id']);
    $this->userLoginFinalize($account);

    return $account;
  }

  /**
   * Save account function.
   *
   * @param UserInterface $account
   * @param array $key_attributes
   *
   * @throws AuthenticationException
   *
   */
  private function saveAccount(UserInterface $account, array $key_attributes) {
    $message = $account->isNew()
      ? 'Saving new account for \'%s\' has failed'
      : 'Updating existing account for \'%s\' has failed';
    $code =
      $account->isNew()
        ? AuthenticationException::USER_CREATION_FAILED
        : AuthenticationException::USER_UPDATE_FAILED;
    if ($account->isNew()) {
      try {
        $account->save();
      }
      catch (EntityStorageException $exception) {
        throw new AuthenticationException(
          sprintf(
            $message,
            $account->getAccountName()),
            $code, $exception
        );
      }
    }
  }

  /**
   * Assert that an existing user is allowed to log in.
   *
   * @param UserInterface $account
   *
   * @throws AuthenticationException
   */
  private function assertExistingUserLoginAllowed(UserInterface $account) {
    if ($account->isBlocked()) {
      throw new AuthenticationException(
        sprintf(
        'User \'%s\' is blocked and cannot be authenticated',
          $account->getAccountName()),
        AuthenticationException::USER_BLOCKED
      );
    }

    foreach ($this->auth_filters as $auth_filter) {
      if (!$auth_filter->isExistingUserLoginAllowed($account)) {
        throw new AuthenticationException(
          $auth_filter->getError(
            AuthFilterPluginInterface::ERROR_EXISTING_NOT_ALLOWED,
            $account),
            AuthenticationException::LOGIN_DISALLOWED_FOR_USER
        );
      }
    }
  }

  /**
   * Assert that user creation is allowed.
   *
   * @throws AuthenticationException
   */
  private function assertUserCreationAllowed($name) {
    foreach ($this->auth_filters as $auth_filter) {
      if (!$auth_filter->isUserCreationAllowed()) {
        $message = $auth_filter
          ->getError(AuthFilterPluginInterface::ERROR_CREATION_NOT_ALLOWED);
        throw new AuthenticationException(
          sprintf('%s, user=\'%s\'', $message, $name),
          AuthenticationException::USER_CREATION_NOT_ALLOWED
        );
      }
    }
  }

  /**
   * Get the key attributes 'session_id', 'name', and 'mail'
   *
   * @return array
   *   An associative array whose keys are:
   *   - session_id
   *   - name
   *   - mail
   *
   * @throws AuthenticationException
   */
  private function getKeyAttributes() {
    $attributes = [];
    foreach (['session_id', 'name', 'mail'] as $id) {
      try {
        $attributes[$id] = $this->attribute_mapper
          ->getAttribute($id, FALSE);
      }
      catch (AttributeException $exception) {
        throw new AuthenticationException(
          sprintf(
            'Missing required attribute \'%s\'',
            $id
          ),
          AuthenticationException::MISSING_ATTRIBUTES, $exception
        );
      }
    }

    return $attributes;
  }
  /**
   * Checks user session and logs the user out if the session is not valid.
   *
   * @param Request $request
   * @param AccountProxyInterface $account
   *
   * @return int
   */
  public function checkUserSession(Request $request, AccountProxyInterface $account): int {
    // Handle anonymous user.
    if ($account->isAnonymous()) {
      if ($this->session_tracker->exists()) {
        $this->session_tracker->clear();
        return self::AUTHCHECK_LOCAL_SESSION_EXPIRED;
      }
      return self::AUTHCHECK_IGNORE;
    }

    // Authenticated user who is not authenticated via shibboleth.
    if (!$this->session_tracker->exists()) {
      return self::AUTHCHECK_IGNORE;
    }

    // Authenticated user with expired shib session.
    $session_id = $this->attribute_mapper->getAttribute('session_id', TRUE);
    if (!$session_id) {
      $this->terminateSession($account);
      return self::AUTHCHECK_SHIB_SESSION_EXPIRED;
    }

    // Authenticated user whose tracked session id does not match the current
    // Session id.
    if ($session_id !== $this->session_tracker->get()) {
      $this->terminateSession($account);
      return self::AUTHCHECK_SHIB_SESSION_ID_MISMATCH;
    }

    // Additional checks by auth filter plugins.
    foreach ($this->auth_filters as $auth_filter) {
      $value = $auth_filter->checkSession($request, $account);
      if ($value !== self::AUTHCHECK_IGNORE) {
        $this->terminateSession($account);
        return $value;
      }
    }

    return self::AUTHCHECK_IGNORE;
  }

  /**
   *
   *
   * @param AccountProxyInterface $account
   */
  private function terminateSession(AccountProxyInterface $account) {
    $this->session_tracker->clear();
    user_logout();
  }

  /**
   * Finalize login.
   *
   * @param UserInterface $account
   */
  private function userLoginFinalize(UserInterface $account) {
    user_login_finalize($account);
  }

  /**
   * {@inheritDoc}
   *
   * @return String login URL
   */
  public function getLoginUrl(): String {
    $loginHandler = $this->handlers['login'];
    $target = '/user';
    $loginUrl = Url::fromUserInput($loginHandler);
    if (!$loginUrl) {
      // @todo Log something about this?
      return 'login_handler_not_valid';
    }

//    $target = $this->redirect_destination->get();
//    if ($target == 'current') {
//      $referer = \Drupal::request()->server->get('HTTP_REFERER');
//      $baseUrl = \Drupal::request()->createFromGlobals()->getSchemeAndHttpHost();
//      $target = substr($referer, strlen($baseUrl));
//    }

    $targetUrl = $this->path_validator->getUrlIfValid('/basicshib/login');
    $targetUrl->setAbsolute(TRUE);
    $targetUrl->setOption('query', ['after_login' => $target]);
    $loginUrl->setOption('query', ['target' => $targetUrl->toString()]);

    return $loginUrl->toString();
  }

}
