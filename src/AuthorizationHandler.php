<?php

namespace Drupal\basicshib;

use Drupal\basicshib\Plugin\BasicShibPluginManager;
use Drupal\basicshib\Plugin\GrouperPluginInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageException;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\user\UserInterface;

/**
 *
 */
class AuthorizationHandler implements AuthorizationHandlerInterface {

  use GrouperHelperTrait;

  /**
   * Grouper plugin instance variable.
   *
   * @var GrouperPluginInterface
   */
  private $grouper;

  /**
   * @var SessionTracker
   */
  private SessionTracker $session_tracker;

  /**
   * @var RequestStack
   */
  protected RequestStack $requestStack;

  protected ImmutableConfig $basicShibSettings;

  protected ImmutableConfig $grouperSettings;
  private ConfigFactoryInterface $config;
  private ImmutableConfig $basicShibAuthFilter;

  /**
   * AuthorizationHandler constructor.
   *
   * @param ConfigFactoryInterface $config_factory
   * @param RequestStack $request_stack
   * @param BasicShibPluginManager $grouper_plugin_manager
   *
   * @throws PluginException
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              RequestStack $request_stack,
                              BasicShibPluginManager $grouper_plugin_manager) {

    $this->requestStack = $request_stack;
    if ($this->requestStack->getCurrentRequest()->hasSession()) {
      $this->session_tracker = new SessionTracker(
        $this->requestStack->getCurrentRequest()->getSession()
      );
    }

    $this->config = $config_factory;
    $this->basicShibSettings = $this->config->get('basicshib.settings');
    $this->basicShibAuthFilter = $this->config->get('basicshib.auth_filter');
    $plugins = $this->basicShibSettings->get('plugins');

    // Create instance of grouper plugin.
    $this->grouper = $grouper_plugin_manager->createInstance($plugins['grouper']);
  }

  /**
   * @param UserInterface $account
   *
   * @throws EntityStorageException
   */
  public function authorize(UserInterface $account)
  {
    $previousRoles = $account->getRoles();

// **** PROD
  $userGroups = explode(';', $this->getUserAttribute('isMemberOf'));
// **** TEST
//    $userGroups = [
//        'urn:mace:duke.edu:groups:core-developers:roles:dws-core-developers',
//        'urn:mace:duke.edu:groups:group-test:roles:dws-core-test',
//        'urn:mace:duke.edu:groups:group-manager:roles:dws-project-managers'
//    ];
// **** END
    $changed = FALSE;
    $roleChanges = $this->getMap($previousRoles, $userGroups);
    foreach ($roleChanges as $key => $changes) {
      $changed = TRUE;
      foreach ($changes as $change) {
        if ($key == 'remove') {
          $account->removeRole($change);
        } else {
          $account->addRole($change);
        }
      }
    }
    if ($changed) {
      $account->save();
    }
  }
}
