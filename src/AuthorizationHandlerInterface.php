<?php

namespace Drupal\basicshib;

use Drupal\user\UserInterface;

/**
 *
 */
interface AuthorizationHandlerInterface {

  /**
   * Attempt to authorize, granting users roles according to their Grouper groups.
   *
   * @param UserInterface $account
   */
  public function authorize(UserInterface $account);

}
