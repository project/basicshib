<?php

declare(strict_types=1);

namespace Drupal\basicshib;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an authorization entity type.
 */
interface AuthorizationInterface extends ConfigEntityInterface {

}
