<?php

namespace Drupal\mymodule\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BasicShibPathProcessor.
 *
 * @package Drupal\basicshib\PathProcessor
 */
class BasicShibPathProcessor implements InboundPathProcessorInterface
{
  protected $basicshib;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->basicshib = $config_factory->get('basicshib.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BasicShibPathProcessor
  {
    return new static(
      $container->get('config.factory'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request)
  {
    if ($path == '/basicshib/login') {
      $request_uri = $request->server->get('REQUEST_URI');
      $target = $request->server->get('target');
      if (!$target) {
        $target = $this->basicshib->get('default_post_login_redirect_path');
      }
      return "/login?destination={$request_uri}{$target}";
    }
  }
}
