<?php

namespace Drupal\basicshib\Controller;

use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\basicshib\AuthorizationHandlerInterface;
use Drupal\basicshib\Exception\AuthenticationException;
use Drupal\basicshib\Exception\RedirectException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\user\Entity\User;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoginController.
 */
class LoginController extends ControllerBase {
  /**
   * Path validator instance.
   *
   * @var PathValidatorInterface
   */
  protected PathValidatorInterface $pathValidator;

  /**
   * Immutable Config instance.
   *
   * @var ImmutableConfig
   */
  protected ImmutableConfig $configuration;

  protected $defaultLogin;
  /**
   * AuthenticationHandler instance.
   *
   * @var AuthenticationHandlerInterface
   */
  protected AuthenticationHandlerInterface $authenticationHandler;

  /**
   * AuthorizationHandler instance.
   *
   * @var AuthorizationHandlerInterface
   */
  protected AuthorizationHandlerInterface $authorizationHandler;

  /**
   * Request instance.
   *
   */
  protected Request $request;

//  /**
//   * True if Grouper plugin enabled, false otherwise.
//   *
//   * @var bool
//   */
//  protected $grouperEnabled;
  private mixed $defaultLogout;

  /**
   * Constructs a new LoginController object.
   *
   * @param AuthenticationHandlerInterface $authentication_handler
   * @param RequestStack $request_stack
   * @param ConfigFactoryInterface $config_factory
   * @param PathValidatorInterface $path_validator
   * @param AuthorizationHandlerInterface $authorization_handler
   *
   */
  public function __construct(
    AuthenticationHandlerInterface $authentication_handler,
    RequestStack $request_stack,
    ConfigFactoryInterface $config_factory,
    PathValidatorInterface $path_validator,
    AuthorizationHandlerInterface $authorization_handler
  )
  {

    $this->request = $request_stack->getCurrentRequest();

    $this->configuration = $config_factory->get('basicshib.settings');
    $this->pathValidator = $path_validator;
    $this->authenticationHandler = $authentication_handler;

    if ($this->configuration->get('plugin_enabled')['grouper_enabled']) {
      $this->authorizationHandler = $authorization_handler;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): LoginController {
    return new static(
      $container->get('basicshib.authentication_handler'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('path.validator'),
      $container->get('basicshib.authorization_handler'),
    );
  }

  /**
   * Login.
   *
   * @return array|RedirectResponse
   *   Either a render array or a redirect response.
   */
  public function login() {
    $exception = NULL;
    $currentAccount = FALSE;
    $authorized = FALSE;
    $messages = $this->configuration->get('messages');

    try {
      // Set the redirect response first so authentication doesn't get
      // processed when the redirect is invalid.
      $redirectResponse = $this->getRedirectResponse();
      // **** TEST
//      $currentAccount = User::load(1);
      // **** PROD
      $currentAccount = $this->authenticationHandler->authenticate();
      // **** END
      if ($this->configuration->get('plugin_enabled')['grouper_enabled'] !== null) {
        $this->authorizationHandler->authorize($currentAccount);
        // ToDo trap any errors thrown during authorization
        $authorized = TRUE;
      }
      $msg = 'User authenticated.';
      $this->logResponse ($currentAccount, $authorized, $msg);
      return $redirectResponse;
    }
    catch (RedirectException $exception) {
      switch ($exception->getCode()) {
        case RedirectException::BLOCKED_EXTERNAL:
          $msg = $messages['external_redirect_error'];
          break;

        case RedirectException::INVALID_PATH:
          $msg = $this->t('Invalid redirect path.');
          break;

        default:
          $msg = $this->t('An unknown redirect error occurred');
          break;
      }
      $this->logResponse ($currentAccount, $authorized, $msg);
      return ['#markup' => $msg];
    }
    catch (AuthenticationException $exception) {
      switch ($exception->getCode()) {
        case AuthenticationException::LOGIN_DISALLOWED_FOR_USER:
          $msg = $messages['login_disallowed_error'];
          break;

        case AuthenticationException::USER_BLOCKED:
          $msg = $messages['account_blocked_error'];
          break;

        case AuthenticationException::USER_CREATION_NOT_ALLOWED:
          $msg = $messages['user_creation_not_allowed_error'];
          break;

        default:
          $msg = $messages['generic_login_error'];
      }
      $this->logResponse ($currentAccount, $authorized, $msg);
      return ['#markup' => $msg];
    }
    finally {
      if ($exception !== null) {
        $this->getLogger('basicshib')->error(
          'Authentication failed: @message -- @backtrace_string',
          Error::decodeException($exception)
        );
      }
    }
  }

  /**
   * Logs out a user.
   *
   * @return Response
   *   The response object.
   */
  public function logout()
  {
    // If basicShib login
    $baseUrl = $this->request->createFromGlobals()->getSchemeAndHttpHost();
    $returnPage = $this->configuration->get('default_post_login_redirect_path');
    if (!empty ($returnPage)) {
      $returnPage = '/' . $returnPage;
    }
    $logoutHandler = $this->configuration->get('handlers')['logout'];
    // check to see if we need to respect a destination specification other than the homepage
    $options['return'] = $baseUrl.'?returnto='.$baseUrl.$returnPage.'/&logoutWithoutPrompt=1';
    user_logout();
    // ToDo change logout routing and log to basicshiblog
    return new Response($baseUrl.$logoutHandler, 200, array('query' => $options, 'absolute' => TRUE));
  }

  /**
   * Get the redirect response.
   *
   * @return RedirectResponse
   *
   * @throws RedirectException
   */
  private function getRedirectResponse(): RedirectResponse
  {
    $redirectPath = $this->request
      ->query
      ->get('after_login');

//    if ($defaultLogin === 'current') {
//      $referer = $this->request->server->get('HTTP_REFERER');
//      $base_url = $this->request->createFromGlobals()->getSchemeAndHttpHost();
//      $redirectPath = substr($referer, strlen($base_url));
//    } else {
//      $redirectPath = $defaultLogin;
//    }

    if ($redirectPath === NULL) {
      $redirectPath = $this->configuration
        ->get('default_post_login_redirect_path');
    }

    $url = $this->pathValidator->getUrlIfValidWithoutAccessCheck($redirectPath);

    if ($url == FALSE) {
      throw new RedirectException(
        $this->t(
          'Redirect path @path is not valid',
          ['@path' => $redirectPath]
        ),
        RedirectException::INVALID_PATH
      );
    }

    if ($url->isExternal()) {
      throw new RedirectException(
        $this->t(
          'Blocked attempt to redirect to external url @url',
          ['@url' => $redirectPath]
        ),
        RedirectException::BLOCKED_EXTERNAL
      );
    }

    try {
      return new RedirectResponse($redirectPath);
    }
    catch (InvalidArgumentException $exception) {
      throw new RedirectException(
        $this->t(
          'Error creating redirect: @message',
          ['@message' => $exception->getMessage()]
        ),
        RedirectException::INVALID_PATH, $exception
      );
    }
  }

  private function logResponse ($account, $authorized, $msg) {
    /*
     * label: uid
     * Status: login approved/rejected
     * Approved: message or reason for rejection
     * Uid: UID of the user logging in or 0 if new user
     * Created: Date login was attempted
     * Changed: Should always be the same as created
     */

  }

}
