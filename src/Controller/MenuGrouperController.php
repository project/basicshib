<?php

namespace Drupal\basicshib\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MenuGrouperController extends ControllerBase
{

  private mixed $enabled;

  public function __construct(ConfigFactoryInterface      $config_factory,
                              TypedConfigManagerInterface $typed_config_manager)
  {
//    parent::__construct($config_factory, $typed_config_manager);
    $this->enabled = $config_factory
      ->get('basicshib.settings')
      ->get('plugin_enabled')['grouper_enabled'];
  }

  /**
   * @param ContainerInterface $container
   *
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
    );
  }

  public function grouperEnabled()
  {
    if ($this->enabled) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();

  }

}
