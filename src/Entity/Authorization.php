<?php

declare(strict_types=1);

namespace Drupal\basicshib\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\basicshib\AuthorizationInterface;

/**
 * Defines the authorization entity type.
 *
 * @ConfigEntityType(
 *   id = "authorization",
 *   label = @Translation("authorization"),
 *   label_collection = @Translation("authorizations"),
 *   label_singular = @Translation("authorization"),
 *   label_plural = @Translation("authorizations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count authorization",
 *     plural = "@count authorizations",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\basicshib\AuthorizationListBuilder",
 *     "form" = {
 *       "add" = "Drupal\basicshib\Form\AuthorizationForm",
 *       "edit" = "Drupal\basicshib\Form\AuthorizationForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "test" = "Drupal\basicshib\Form\AuthorizationTestForm",
 *     },
 *   },
 *   config_prefix = "authorization",
 *   admin_permission = "administer authorization",
 *   links = {
 *     "collection" = "/admin/config/authorization",
 *     "add-form" = "/admin/config/authorization/add",
 *     "edit-form" = "/admin/config/authorization/{authorization}",
 *     "delete-form" = "/admin/config/authorization/{authorization}/delete",
 *     "test-form" = "/admin/config/authorization/test",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "policy" = "policy",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "policy",
 *     "description",
 *   },
 * )
 */
final class Authorization extends ConfigEntityBase implements AuthorizationInterface {

  /**
   * The authorization ID.
   */
  protected string $id;

  /**
   * The authorization label.
   */
  protected string $label;

  /**
   * The authorization policy.
   */
  protected string $policy;

  /**
   * The authorization description.
   */
  protected string $description;

}
