<?php

declare(strict_types=1);

namespace Drupal\basicshib\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\basicshib\PoliciesInterface;

/**
 * Defines the policies entity type.
 *
 * @ConfigEntityType(
 *   id = "policies",
 *   label = @Translation("Policies"),
 *   label_collection = @Translation("Policies"),
 *   label_singular = @Translation("policies"),
 *   label_plural = @Translation("policiess"),
 *   label_count = @PluralTranslation(
 *     singular = "@count policy",
 *     plural = "@count policies",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\basicshib\PoliciesListBuilder",
 *     "form" = {
 *       "add" = "Drupal\basicshib\Form\PoliciesForm",
 *       "edit" = "Drupal\basicshib\Form\PoliciesForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "policies",
 *   admin_permission = "administer policies",
 *   links = {
 *     "collection" = "/admin/config/policies",
 *     "add-form" = "/admin/config/policies/add",
 *     "edit-form" = "/admin/config/policies/{policies}",
 *     "delete-form" = "/admin/config/policies/{policies}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "policy" = "policy",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "policy",
 *     "description",
 *   },
 * )
 */
final class Policies extends ConfigEntityBase implements PoliciesInterface {

  /**
   * The example ID.
   */
  protected string $id;


  protected string $label;

  /**
   * The example label.
   */
  protected string $policy;

  /**
   * The example description.
   */
  protected string $description;

}
