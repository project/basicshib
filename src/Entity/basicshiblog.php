<?php

namespace Drupal\basicshib\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\basicshib\basicshiblogInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the basicshiblog entity class.
 *
 * @ContentEntityType(
 *   id = "basicshiblog",
 *   label = @Translation("basicshiblog"),
 *   label_collection = @Translation("basicshiblog"),
 *   label_singular = @Translation("basicshiblog"),
 *   label_plural = @Translation("basicshiblog"),
 *   label_count = @PluralTranslation(
 *     singular = "@count basicshiblog",
 *     plural = "@count basicshiblog",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\basicshib\basicshiblogListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\basicshib\basicshiblogAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\basicshib\Form\basicshiblogForm",
 *       "edit" = "Drupal\basicshib\Form\basicshiblogForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *   },
 *   base_table = "basicshiblog",
 *   admin_permission = "administer basicshiblog",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "response" = "response",
 *   },
 *   links = {
 *     "collection" = "/admin/content/basicshiblog",
 *     "add-form" = "/basicshiblog/add",
 *     "canonical" = "/basicshiblog/{basicshiblog}",
 *     "edit-form" = "/basicshiblog/{basicshiblog}/edit",
 *     "delete-form" = "/basicshiblog/{basicshiblog}/delete",
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "response" = "response",
 *   }
 * )
 */
class basicshiblog extends ContentEntityBase implements basicshiblogInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array
  {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['response'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Response'))
      ->setDescription(t('System response returned at login.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the basicshiblog was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
