<?php

namespace Drupal\basicshib\EventSubscriber;

use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 *
 */
class RequestEventSubscriber implements EventSubscriberInterface {

  /**
   * Instance variable representing the current user.
   *
   * @var AccountProxyInterface
   */
  private AccountProxyInterface $current_user;

  /**
   * Instance variable representing the logger channel factory.
   *
   * @var LoggerChannelFactoryInterface
   */
  private $logger_channel_factory;

  /**
   * Instance variable representing the authentication handler.
   *
   * @var AuthenticationHandlerInterface
   */
  private AuthenticationHandlerInterface $authentication_handler;

  /**
   * AuthEventSubscriber constructor.
   *
   * @param AccountProxyInterface $current_user
   * @param LoggerChannelFactoryInterface $logger_channel_factory
   * @param AuthenticationHandlerInterface $authentication_handler
   */
  public function __construct(AccountProxyInterface $current_user,
                              LoggerChannelFactoryInterface $logger_channel_factory,
                              AuthenticationHandlerInterface $authentication_handler
  )
  {
    $this->current_user = $current_user;
    $this->logger_channel_factory = $logger_channel_factory;
    $this->authentication_handler = $authentication_handler;
  }

  /**
   * {@inheritdoc}
   *
   * @return array list of events.
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['onRequest', 100];
    return $events;
  }

  /**
   * Request handler.
   *
   * @param RequestEvent $event
   * @param $eventId
   * @param EventDispatcher $dispatcher
   *
   */
  public function onRequest(RequestEvent $event) {
    $this->authentication_handler->checkUserSession(
      $event->getRequest(),
      $this->current_user
    );
  }

}
