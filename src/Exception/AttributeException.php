<?php

namespace Drupal\basicshib\Exception;

/**
 * Create constants that represent exceptions.
 */
class AttributeException extends BasicShibException {
  const NOT_MAPPED = 1;
  const NOT_SET = 2;
  const DUPLICATE_ID = 3;

}
