<?php

namespace Drupal\basicshib\Exception;

/**
 * Creates constants that represent basicshib exceptions .
 */
class BasicShibException extends \Exception {

}
