<?php

namespace Drupal\basicshib\Exception;

/**
 * Creates constant that represent Redirect Exceptions.
 */
class RedirectException extends BasicShibException {
  const BLOCKED_EXTERNAL = 1;
  const INVALID_PATH = 2;

}
