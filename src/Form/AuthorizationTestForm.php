<?php

declare(strict_types=1);

namespace Drupal\basicshib\Form;

use Drupal\basicshib\Plugin\BasicShibPluginManager;
use Drupal\basicshib\SessionTracker;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\basicshib\Entity\Authorization;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * authorization test form.
 */

final class AuthorizationTestForm extends EntityForm
{
  private object $grouper;
  private AccountInterface $account;
  private SessionTracker $session_tracker;
  private array $policies;
  private ConfigFactoryInterface $config;
  private $roles;

  /**
   * @param ConfigFactoryInterface $config_factory
   * @param BasicShibPluginManager $grouper_plugin_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              RequestStack $request_stack,
                              BasicShibPluginManager $grouper_plugin_manager,
                              AccountInterface $account
  ) {
    $this->requestStack = $request_stack;
    if ($this->requestStack->getCurrentRequest()->hasSession()) {
      $this->session_tracker = new SessionTracker(
        $this->requestStack->getCurrentRequest()->getSession()
      );
    }
    $this->config = $config_factory;
    $plugins = $config_factory->get('basicshib.settings')->get('plugins');
    $this->policies = $config_factory->listAll($prefix = "basicshib.policies");

    // Create instance of grouper plugin.
    $this->grouper = $grouper_plugin_manager->createInstance($plugins['grouper']);
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('plugin.manager.basicshib.grouper'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array
  {

    $account = $this->account;
    $form = parent::form($form, $form_state);

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => 'User',
      '#maxlength' => 255,
      '#default_value' => $account->getAccountName(),
      '#required' => TRUE,
    ];

    $roles = $this->grouper->getUserRoles($account);
    $this->roles = $roles;
    $currentRoles = implode("\n", $roles);
    $form['current_roles'] = [
      '#type' => 'textarea',
      '#title' => 'Current Roles',
      '#maxlength' => 255,
      '#default_value' => $currentRoles,
      '#required' => TRUE,
    ];

    $currentPolicies = [];

    $allPolicies = '';
    foreach ($roles as $key => $role) {
      $allPolicies .= $role . "\n";
      $allPolicies .= "\t" . $this->config->get("basicshib.authorization." . $role)->getName() . "\n";
      $allPolicies .= "\t\t" . $this->config->get("basicshib.authorization." . $role)->get("policy") . "\n";
      $allPolicies .= "\n";
    }
    $form['policies'] = [
      '#type' => 'textarea',
      '#title' => 'Policies',
      '#maxlength' => 255,
      '#default_value' => $allPolicies,
      '#required' => TRUE,
    ];

    $currentRoles = $this->authorizeDummy($roles);

    $form['new_roles'] = [
      '#type' => 'textarea',
      '#title' => 'New Roles',
      '#maxlength' => 255,
      '#default_value' => $currentRoles,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
//    $result = parent::save($form, $form_state);
//    $message_args = ['%label' => $this->entity->label()];
//    $this->messenger()->addStatus(
//      match ($result) {
//        \SAVED_NEW => $this->t('Created new authorization %label.', $message_args),
//        \SAVED_UPDATED => $this->t('Updated authorization %label.', $message_args),
//      }
//    );
//    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
//    return $result;
  }

  public function authorizeDummy($roles)
  {
    $grouperGroups = [
      'admin-grouper-policy',
      'staff-grouper-policy',
      'student-grouper-policy',
      'alumni-grouper-policy',
      //     'authenticated-grouper-policy'
    ];
    $newRoles = "";
    foreach ($roles as $role) {
      if ($role != 'authenticated') {
        $approved = FALSE;
        foreach ($grouperGroups as $grouperGroup) {
          $policyName = $this->config->get("basicshib.authorization." . $role)->getName();
          $policyGroup = $this->config->get("basicshib.authorization." . $role)->get("policy");
          if (str_contains($policyGroup, $grouperGroup)) {
            $approved = TRUE;
            break;
          }
        }
        if ($approved) {
          $newRoles .= $role . "\n";
        }
      }
    }

    return $newRoles;
  }

  public function getMap($userRoles) {
    $role_map = NULL;
    $drupal_roles = $this->getDrupalRoles();
    for ($i = 0; $i < count($drupal_roles); $i++) {
      $role = $this->configuration->get('role_' . $i);
      $key = $drupal_roles[$i];
      $role_map['' . $key] = explode(';', $role);
    }

    $role_map_keys = array_keys($role_map);
    for ($i = 0; $i < count($role_map_keys); $i++) {
      $roles = $role_map_keys[$i];
      for ($j = 0; $j < count($role_map[$roles]); $j++) {
        $group_path = $role_map[$roles][$j];
        $this->map[$group_path] = $roles;
      }
    }
    return $this->map;
  }

  public static function getDrupalRoles(array $ignoreRoles = []): array {
    $definedRoles = array_keys(Role::loadMultiple());
    $validRoles = [];
    foreach ($definedRoles as $role) {
      if (!in_array ($role, $ignoreRoles)) {
        $validRoles[] = $role;
      }
    }

    return $validRoles;
  }

}

