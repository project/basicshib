<?php

namespace Drupal\basicshib\Form;

use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\basicshib\GrouperHelperTrait;

/**
 * Class CoreSettingsForm.
 */
class GrouperSettingsForm extends ConfigFormBase {
  use GrouperHelperTrait;

  /**
   * The User's Drupal roles.
   *
   * @var array
   */
  protected array $roles;
  private $authorizationHandler;

  /**
   * Constructs a new GrouperSettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->roles = $this->getDrupalRoles();
  }

  /**
   * @param ContainerInterface $container
   *
   * @return GrouperSettingsForm
   */
  public static function create(ContainerInterface $container): GrouperSettingsForm {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'basicshib.settings',
      'basicshib.auth_filter',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'grouper_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $types = [];
    $types['auth_filter'] = \Drupal::service('plugin.manager.basicshib.auth_filter');
    $types['grouper'] = \Drupal::service('plugin.manager.basicshib.grouper');
    $types['user_provider'] = \Drupal::service('plugin.manager.basicshib.user_provider');
    foreach ($types as $type) {
      $definitions = $type->getDefinitions();
    }
//    $type['user_provider']['definitions'] = $type['auth_filter']->getDefinitions();
//    foreach($plugin_definitions as $plugin_id => $plugin_definition) {
//    $plugin_definition = $type->getDefinition($plugin_id);


    $form['grouper_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grouper Enabled'),
      '#default_value' => $this->config('basicshib.settings')
        ->get('plugin_enabled')['grouper_enabled'],
    ];

    $form['account_provisioning'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Account Provisioning'),
      '#states' => [
        'visible' => [
          ':input[name="grouper_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['account_provisioning']['create_user'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create New User at Log-In'),
      '#default_value' => $this->config('basicshib.auth_filter')
        ->get('create_user')['allow'],
      '#description'  => $this->t('Create User at log-in if they do not exist.'),
    ];

    $form['account_provisioning']['remove_authenticated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove User at Log-In'),
      '#default_value' => $this->config('basicshib.auth_filter')
        ->get('remove_authenticated')['allow'],
      '#description'  => $this->t('Remove User at log-in if they only have Authenticated Role.'),
    ];

    $form['account_provisioning']['remove_administrator'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove Administrator Role at Log-In'),
      '#default_value' => $this->config('basicshib.auth_filter')
        ->get('remove_administrator')['allow'],
      '#description'  => $this->t('Remove Administrator Role at log-in if not assigned by Grouper.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('basicshib.settings')
      ->set('plugin_enabled', ['grouper_enabled' => $form_state->getValue('grouper_enabled')]);
    $this->config('basicshib.settings')->save();

    $this->config('basicshib.auth_filter')
      ->set('create_user', ['allow' => $form_state->getValue('create_user')]);
    $this->config('basicshib.auth_filter')
      ->set('remove_authenticated', ['allow' => $form_state->getValue('remove_authenticated')]);
    $this->config('basicshib.auth_filter')
      ->set('remove_administrator', ['allow' => $form_state->getValue('remove_administrator')]);
    $this->config('basicshib.auth_filter')->save();

    \Drupal::service("router.builder")->rebuild();
  }

}
