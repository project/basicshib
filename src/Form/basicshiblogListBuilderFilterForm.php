<?php

namespace Drupal\basicshib\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a BasicShib form.
 */
class basicshiblogListBuilderFilterForm extends FormBase {

  protected array $messages;
  protected EntityTypeManager $entityTypeManager;
  /**
   * @var EntityStorageInterface|mixed|object
   */
  protected $entity;

  /**
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              EntityTypeManager      $entity_type_manager)
  {
    $this->messages = array_flip($config_factory->get('basicshib.settings')->get('messages'));
    $this->entity = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): basicshiblogListBuilderFilterForm
  {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'basicshib_basic_shib_log_list_builder_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $form['filter'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
    ];

    $form['filter']['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display User login/logouts, (uid)'),
      '#size' => 10,
      '#required' => FALSE,
    ];

    $form['filter']['date_from'] = [
      '#type' => 'date',
      '#title' => $this->t('From Date'),
      '#required' => FALSE,
    ];

    $form['filter']['date_to'] = [
      '#type' => 'date',
      '#title' => $this->t('To Date'),
      '#required' => FALSE,
    ];

    $options = array_merge (['approved' => 'Approved', 'rejected' => 'Rejected'] + $this->messages);
    $form['filter']['response'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Display responses'),
      '#description' => $this->t('No selection will return ALL records.'),
      '#multiple' => TRUE,
      '#required' => FALSE,
    ];

    $form['filter']['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
//    if (mb_strlen($form_state->getValue('message')) < 10) {
//      $form_state->setErrorByName('message', $this->t('Message should be at least 10 characters.'));
//    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [
      'user' => $form_state->getValue('user'),
      'date_from' => $form_state->getValue('date_from'),
      'date_to' => $form_state->getValue('date_to'),
      'response' => $form_state->getValue('response'),
    ];
    $form_state->setRedirect('entity.basicshiblog.collection', $query);
  }

}
