<?php

declare(strict_types=1);

namespace Drupal\basicshib;

use Drupal;
use Drupal\user\Entity\Role;

/**
 *
 */
trait GrouperHelperTrait {

  /**
   * Queries available Drupal roles from the specific website.
   * returns an array with the Drupal roles excluding roles passed in the ignoreRoles array.
   *
   * @return array
   */
  public static function getDrupalRoles(array $ignoreRoles = []): array {
    $definedRoles = array_keys(Role::loadMultiple());
    $validRoles = [];
    foreach ($definedRoles as $role) {
      if (!in_array ($role, $ignoreRoles)) {
        $validRoles[] = $role;
      }
    }

    return $validRoles;
  }

    /**
   * Queries available Grouper Policies from the specific website.
   * returns an array of all the policies.
   *
   * @return
   */
  public static function getGrouperPolicies() {
    $grouperPolicies = \Drupal::config('basicshib.Grouper Policies.drupal_admin');

    return $grouperPolicies;
  }

  /**
   * Returns a user attribute specified by a string.
   *
   * @param string
   *   denotes the desired attribute name
   *
   * @return string
   */
  protected function getUserAttribute($attribute_name): ?string {
    $req = Drupal::request();
    return $req->server->get($attribute_name);
  }

  public function getMap($previousRoles, $userGroups): array
  {
//    // Get the policies for the users current roles
//    $rolePolicies = [];
//    foreach ($previousRoles as $key => $role) {
//      $policies = $this->config->get("basicshib.authorization." . $role)->get("policy");
//      if (!$policies) {
//        $policies = 'none';
//      }
//      $rolePolicies[$role] = $policies;
//    }

    $roleChanges = [];
    // REMOVE RECENTLY UNASSIGNED ROLES
    // Get the policies that grouper has assigned to the user
    foreach ($previousRoles as $role) {
      $found = FALSE;
      foreach ($userGroups as $userGroup) {
        $policyName = $this->config->get("basicshib.authorization." . $role)->getName();
        $policyGroup = $this->config->get("basicshib.authorization." . $role)->get("policy");
        // If any of the required groups are found then the user stays assigned to the role
        if ($policyGroup != NULL && str_contains($policyGroup, $userGroup)) {
          $found = TRUE;
        }
      }
      if (!$found) {
        if (($role == 'administrator' && $this->basicShibAuthFilter->get('remove_administrator')['allow'] == FALSE) ||
          ($role == 'authenticated' && $this->basicShibAuthFilter->get('remove_authenticated')['allow'] == FALSE)) {
          continue;
        }
        $roleChanges['remove'][] = $role;
      }
    }

    // ADD NEWLY ASSIGNED ROLES
    $rolePrefix = 'user.role';
    $roleNames = $this->config->listAll($rolePrefix);
    foreach ($roleNames as $roleName) {
      $role = substr($roleName, strlen($rolePrefix) +1);
      if (!in_array ($role, $previousRoles)) {
        foreach ($userGroups as $userGroup) {
          $policyName = $this->config->get("basicshib.authorization." . $role)->getName();
          $policyGroup = $this->config->get("basicshib.authorization." . $role)->get("policy");
          if ($policyGroup != NULL) {
            if (str_contains($policyGroup, $userGroup)) {
              $roleChanges['add'][] = $role;
            }
          }
        }
      }
    }

    return $roleChanges;
  }
}
