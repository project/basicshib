<?php

namespace Drupal\basicshib;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a grouper entity type.
 */
interface GrouperRoleInterface extends ConfigEntityInterface {

  public function getGroupsByRoleId(string $id);

  public function getGroupsByRoleLabel(string $id);

  public function getAllGroups();
}
