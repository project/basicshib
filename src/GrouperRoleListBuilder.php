<?php

namespace Drupal\basicshib;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Grouper Roles.
 */
class GrouperRoleListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Role');
    $header['id'] = $this->t('Role ID');
    $header['groups'] = $this->t('Groups');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var GrouperRoleInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['groups'] = $entity->get('groups');
    return $row + parent::buildRow($entity);
  }

}
