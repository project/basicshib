<?php

namespace Drupal\basicshib\Plugin;

use Drupal\basicshib\Annotation\BasicShibAuthFilter;
use Drupal\basicshib\Annotation\BasicShibUserProvider;
use Drupal\basicshib\Annotation\BasicShibGrouper;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 *
 */
class BasicShibPluginManager extends DefaultPluginManager {

  /**
   *
   */
  public function __construct(
        $type,
        \Traversable $namespaces,
        CacheBackendInterface $cache_backend,
        ModuleHandlerInterface $module_handler
    ) {
    $type_annotations = [
      'user_provider' => BasicShibUserProvider::class,
      'auth_filter' => BasicShibAuthFilter::class,
      'grouper' => BasicShibGrouper::class,
    ];

    $type_plugins = [
      'user_provider' => UserProviderPluginInterface::class,
      'auth_filter' => AuthFilterPluginInterface::class,
      'grouper' => GrouperPluginInterface::class,
    ];

    parent::__construct(
          'Plugin/basicshib/' . $type,
          $namespaces, $module_handler,
          $type_plugins[$type],
          $type_annotations[$type]
      );
    $this->setCacheBackend($cache_backend, 'basicshib_' . $type . '_plugins');
  }

}
