<?php

namespace Drupal\basicshib\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DynamicLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var ConfigFactory
   */
  protected ConfigFactory|ImmutableConfig $config;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('basicshib.grouper_settings');
  }

  public static function create(ContainerInterface $container, $base_plugin_id): DynamicLocalTasks {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Hide or display the grouper tab depending on if the 'Enable Grouper'
   * checkbox is set.
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $baseRoute = 'entity.authorization.collection';
//    title: 'Grouper settings'
//#  route_name: basicshib.grouper_settings_form
//#  description: 'Configure the Grouper plugin'
//#  base_route: basicshib.core_settings_form
//    $settings = $this->configFactory->get('basicshi
//b.settings');
//
//    if ($settings->get('plugin_enabled')['grouper_enabled']) {
//      $baseRoute = 'basicshib.grouper_settings_form';
//    }
    $this->derivatives['basicshib.grouper_settings_form'] = $base_plugin_definition;
    $this->derivatives['basicshib.grouper_settings_form']['title'] = "Authorizations";
    $this->derivatives['basicshib.grouper_settings_form']['route_name'] = 'entity.authorization.collection';
//    $this->derivatives['basicshib.grouper_settings_form']['description'] = 'Configure the Grouper plugin';
    $this->derivatives['basicshib.grouper_settings_form']['base_route'] = $baseRoute;
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
