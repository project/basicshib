<?php

namespace Drupal\basicshib\Plugin;

use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 *
 */
interface GrouperPluginInterface {

  /**

   * @param UserInterface $account
   *
   * @return array
   */
  public function getUserRoles($account);

  /**
   * Returns a map with key values of grouper groups and values of Drupal roles.
   *
   * This information is managed by the website developer.
   *
   * @return array
   */
  public function getMap($userRoles);

}
