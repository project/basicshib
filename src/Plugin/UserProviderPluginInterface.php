<?php

namespace Drupal\basicshib\Plugin;

/**
 *
 */
interface UserProviderPluginInterface {

  /**
   * Load an existing user.
   *
   * @param string $name
   *
   * @return \Drupal\user\UserInterface|null
   */
  public function loadUserByName($name);

  /**
   * Create a new user. The user will be saved by the login controller.
   *
   * @param string $name
   * @param string $mail
   *
   * @return \Drupal\user\UserInterface|null
   *   The user, or null if one cannot be created.
   */
  public function createUser($name, $mail);

}
