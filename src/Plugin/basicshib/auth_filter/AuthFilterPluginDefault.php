<?php

namespace Drupal\basicshib\Plugin\basicshib\auth_filter;

use Drupal\basicshib\AuthenticationHandler;
use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\basicshib\Plugin\AuthFilterPluginInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthenticationFilterPluginDefault.
 *
 * @package Drupal\basicshib\Plugin\basicshib\auth_filter
 *
 * @BasicShibAuthFilter(
 *   id = "basicshib",
 *   title = "Basic authentication filter"
 * )
 */
class AuthFilterPluginDefault implements AuthFilterPluginInterface, ContainerFactoryPluginInterface {
  /**
   * @var ImmutableConfig
   */
  private ImmutableConfig $auth_filterConfig;

  /**
   * AuthenticationFilterPluginDefault constructor.
   *
   * @param ImmutableConfig $auth_filterConfig
   */
  public function __construct(ImmutableConfig $auth_filterConfig) {
    $this->auth_filterConfig = $auth_filterConfig;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $container->get('config.factory')->get('basicshib.auth_filter')
      );
  }

  /**
   * @inheritDoc
   */
  public function isUserCreationAllowed() {
    $create_filter = $this->auth_filterConfig
      ->get('create_user');
    return $create_filter['allow'];
  }

  /**
   * @inheritDoc
   */
  public function isExistingUserLoginAllowed(UserInterface $account) {
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function getError($code, UserInterface $account = NULL) {
    switch ($code) {
      case self::ERROR_CREATION_NOT_ALLOWED:
        $create_filter = $this->auth_filterConfig
          ->get('create_user');
        return $create_filter['error'];
    }
  }

  /**
   * @inheritDoc
   */
  public function checkSession(Request $request, AccountProxyInterface $account) {
    return AuthenticationHandlerInterface::AUTHCHECK_IGNORE;
  }

}
