<?php

namespace Drupal\basicshib\Plugin\basicshib\grouper;

use Drupal\basicshib\Plugin\GrouperPluginInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\basicshib\GrouperHelperTrait;

/**
 * Class GrouperPluginDefault.
 *
 * @package Drupal\basicshib\Plugin\basicshib\grouper
 *
 * @BasicShibGrouper(
 *   id = "grouper_default",
 *   title = "grouper"
 * )
 */
class GrouperPluginDefault implements GrouperPluginInterface, ContainerFactoryPluginInterface {
  use GrouperHelperTrait;

  /**
   * Instance variable configuration.
   *
   * @var ImmutableConfig
   */
  private $configuration;

  /**
   * Group to Role map.
   */
  private $map;
  private $userRoles;

  /**
   * GrouperPluginDefault constructor.
   *
   * @param ImmutableConfig $configuration
   */
  public function __construct(ImmutableConfig $configuration) {
    $this->configuration = $configuration;
    $this->map = [];
  }

  /**
   * {@inheritDoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param string $plugin_definition
   *
   * @return GrouperPluginDefault instance
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory')->get('basicshib.grouper_settings')
    );
  }

  public function getUserRoles($account) {

    $this->userRoles = $account->getRoles();

    return $this->userRoles;
  }

  /**
   * {@inheritDoc}
   *
   * @return array with keys as grouper groups and values as Drupal roles.
   */
  public function getMap($userRoles) {
    $role_map = NULL;
    $drupal_roles = $this->getDrupalRoles();
    for ($i = 0; $i < count($drupal_roles); $i++) {
      $role = $this->configuration->get('role_' . $i);
      $key = $drupal_roles[$i];
      if (!empty($role)) {
        $role_map['' . $key] = explode(';', $role);
      }
    }

    if (!empty($role_map)) {
      $role_map_keys = array_keys($role_map);
      for ($i = 0; $i < count($role_map_keys); $i++) {
        $roles = $role_map_keys[$i];
        for ($j = 0; $j < count($role_map[$roles]); $j++) {
          $group_path = $role_map[$roles][$j];
          $this->map[$group_path] = $roles;
        }
      }
    }
    return $this->map;
  }

}
