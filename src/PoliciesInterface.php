<?php

declare(strict_types=1);

namespace Drupal\basicshib;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a policies entity type.
 */
interface PoliciesInterface extends ConfigEntityInterface {

}
