<?php

namespace Drupal\basicshib\Routing;

use Drupal\basicshib\AuthenticationHandler;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\core\Routing\RoutingEvents;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  protected ImmutableConfig $basicshib;
  protected PathValidatorInterface $path_validator;
  private $authenticationHandler;

  public function __construct(
    ConfigFactoryInterface $config,
    PathValidatorInterface $path_validator,
    AuthenticationHandler $basicshib_authentication_handler
  ) {
    $this->basicshib = $config->get('basicshib.settings');
    $this->path_validator = $path_validator;
    $this->authenticationHandler = $basicshib_authentication_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): RouteSubscriber {
    return new static(
      $container->get('config.factory'),
      $container->get('path.validator'),
      $container->get('basicshib.authentication_handler')
    );
  }

  protected function alterRoutes(RouteCollection $collection): void
  {
    // If Shibboleth module enabled and shibboleth log-in enabled, set login link and label to Shibboleth defaults.
//    if (\Drupal::service('path.validator')->getUrlIfValid('/basicshib/login')) {
//      $route = $collection->get('user.login');
//      $config = \Drupal::config('basicshib.settings');
//      if ($config->get('shibboleth_enabled') == TRUE) {
//        $url = $this->authenticationHandler->getLoginUrl();
//        $route->setPath($this->authenticationHandler->getLoginUrl());
//        $route->setDefault('_title', $config->get('login_link_label'));
//      } else {
//        $url = $this->authenticationHandler->getLoginUrl();
//        $route->setPath('/user/login');
//        $route->setDefault('_title', 'Log in');
//      }
//    }
  }

}
