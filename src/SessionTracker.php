<?php

namespace Drupal\basicshib;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class SessionTracker.
 *
 * Tracks the shibboleth session id that was used for login.
 *
 * @package Drupal\basicshib
 */
class SessionTracker {
  const VARNAME = 'basicshib_tracked$_session_id';

  /**
   * @var SessionInterface|null
   */
  private?SessionInterface $session;

  /**
   * SessionTracker constructor.
   *
   * @param SessionInterface|null $session
   */
  public function __construct(?SessionInterface $session = NULL) {
    $this->session = $session;
  }

  /**
   * Get the session id.
   *
   * @return string|null
   *   The tracked session id.
   */
  public function get(): ?string {
    return $this->session !== NULL
        ? $this->session->get(self::VARNAME)
        : NULL;
  }

  /**
   * Set the session id.
   *
   * @param string|null $value
   *   The value to set, or NULL to remove the session entry.
   */
  public function set(string $value) {
    if ($this->session !== NULL) {
      $this->session->set(self::VARNAME, $value);
    }
  }

  /**
   * Clear the tracked session id.
   */
  public function clear() {
    if ($this->session) {
      $this->session->remove(self::VARNAME);
    }
  }

  /**
   * @return bool
   */
  public function exists(): bool {
    if ($this->session) {
      return $this->session->has(self::VARNAME);
    }

    return FALSE;
  }

}
