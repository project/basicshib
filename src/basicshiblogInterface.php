<?php

namespace Drupal\basicshib;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a basicshiblog entity type.
 */
interface basicshiblogInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
