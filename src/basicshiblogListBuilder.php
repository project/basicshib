<?php

namespace Drupal\basicshib;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a list controller for the basicshiblog entity type.
 */
class basicshiblogListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;
  protected array $messages;
  protected EntityTypeInterface $users;
  protected UserStorageInterface $userStorage;
  protected RequestStack $requestStack;
  protected FormBuilderInterface $formBuilder;
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new basicshiblogListBuilder object.
   *
   * @param EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param EntityTypeManagerInterface $entity_type_manager
   *  The
   * @param DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param RequestStack $request_stack
   *  The current request
   * @param FormBuilderInterface $form_builder
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityTypeManagerInterface $entity_type_manager,
    DateFormatterInterface $date_formatter,
    RequestStack $request_stack,
    FormBuilderInterface $form_builder
  ) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($entity_type, $this->entityTypeManager->getStorage($entity_type->id()));
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
    $this->formBuilder = $form_builder;

  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('request_stack'),
      $container->get('form_builder')
    );
  }

  protected function getEntityIds() {
    $request = $this->requestStack->getCurrentRequest();
    $user = $request->get('user');
    $date_from = $request->get('date_from');
    $date_to = $request->get('date_to');
    $response = $request->get('response');

    $query = $this->entityTypeManager
      ->getStorage($this->entityTypeId)
      ->getQuery()
      ->accessCheck(TRUE);
    if ($user) {
      $query->condition('id', $user);
    }
    if ($date_from) {
      $query->condition('created', $date_from);
    }
    if ($date_to) {
      $query->condition('created', $date_from, '>=');
      $query->condition('created', $date_to, '<=');
    }
    if ($response) {
      $group = $query->orConditionGroup();
      foreach ($response as $type) {
        $group->condition('response', $type);
      }
      $query->condition($group);
    }

    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = $this->formBuilder->getForm('Drupal\basicshib\Form\basicshiblogListBuilderFilterForm');
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total BasicShib Log entries: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['uid'] = $this->t('Author');
    $header['response'] = $this->t('Response');
    $header['created'] = $this->t('Created');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var basicshiblogInterface $entity */
//    $row['id'] = $entity->id();
//    $row['label'] = $entity->toLink();
    $row['id'] = $entity->toLink();
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    $row['response'] = $entity->get('response');
    $row['created'] = $this->dateFormatter->format($entity->get('created')->value);
    return $row + parent::buildRow($entity);
  }

}
