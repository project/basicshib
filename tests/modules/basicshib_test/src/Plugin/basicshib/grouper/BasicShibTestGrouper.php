<?php

namespace Drupal\basicshib_test\Plugin\basicshib\grouper;

use Drupal\basicshib\GrouperHelperTrait;
use Drupal\basicshib\Plugin\AuthFilterPluginInterface;
use Drupal\basicshib\Plugin\GrouperPluginInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @BasicShibGrouper(
 *   id = "basicshib_test",
 *   title = "Test Grouper"
 * )
 */
class BasicShibTestGrouper implements GrouperPluginInterface, ContainerFactoryPluginInterface {
  use GrouperHelperTrait;

  /**
   * @var ImmutableConfig
   */
  private ImmutableConfig $configuration;

  /**
   * GrouperPluginDefault constructor.
   *
   * @param ImmutableConfig $configuration
   */
  public function __construct(ImmutableConfig $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param string $plugin_definition
   *
   * @return \Drupal\basicshib\Plugin\basicshib\grouper\GrouperPluginDefault instance
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): GrouperPluginDefault {
    return new static(
      $container
        ->get('config.factory')
        ->get('basicshib.grouper_settings')
    );
  }

  /**
   * {@inheritDoc}
   *
   * @return array a map with keys as grouper groups and values as Drupal roles.
   */
  public function getMap(): array {

    $this->container->get('basicshib.grouper_plugin_default');
    $drupal_roles = $this->configuration->get();
      return $this->map;
  }
}
