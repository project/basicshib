<?php

namespace Drupal\Tests\basicshib\Kernel;

use Drupal;
use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\basicshib\Exception\AttributeException;
use Drupal\basicshib\Exception\AuthenticationException;
use Drupal\basicshib\SessionTracker;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\basicshib\Traits\MockTrait;
use Drupal\user\UserInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 *
 */
class AuthenticationHandlerTest extends KernelTestBase {
  use MockTrait;

  protected static $modules = ['basicshib', 'user', 'system'];

  /**
   *
   */
  public function setUp(): void{
    parent::setUp();
    $this->installConfig(['basicshib']);
    $this->installEntitySchema('user');
    $this->installSchema('system', 'sequences');
  }

  /**
   * @param $name
   * @param $mail
   * @param int $status
   *
   * @return EntityInterface
   * @throws EntityStorageException
   */
  private function createUser($name, $mail, int $status = 1): EntityInterface {
    /**
     * @var \Drupal\user\UserStorageInterface $storage
     */
    $storage = $this->container->get('entity_type.manager')
      ->getStorage('user');

    $user = $storage->create(
          [
            'name' => $name,
            'mail' => $mail,
            'status' => $status,
          ]
      );

    $user->save();

    return $user;
  }

  /**
   *
   * @throws \Exception
   */
  public function testAuthenticationWithExistingUserSucceeds() {
    $user = $this->createUser('jdoe@example.com', 'jdoe@mail.example.com');

    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => 'abcd',
            'eppn' => $user->getAccountName(),
            'mail' => $user->getEmail(),
          ]
      );

    $this->container->set('request_stack', $request_stack);

    $handler = $this->container->get('basicshib.authentication_handler');

    try {
      $handler->authenticate();
    }
    catch (Exception $exception) {
      $this->fail(sprintf('An exception was thrown: %s', $exception->getTraceAsString()));
    }
  }

  /**
   *
   * @throws \Exception
   */
  public function testAuthenticationWithNewUserFails() {
    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => 'abcd',
            'eppn' => 'jdoe@example.com',
            'mail' => 'jdoe@mail.example.com',
          ]
      );

    $this->container->set('request_stack', $request_stack);

    $handler = $this->container->get('basicshib.authentication_handler');

    try {
      $handler->authenticate();
      $this->fail('Authenticate succeeded, but was not expected to');
    }
    catch (AuthenticationException $exception) {
      self::assertEquals(AuthenticationException::USER_CREATION_NOT_ALLOWED, $exception->getCode());
    }
  }

  /**
   *
   * @throws \Exception
   * @throws \Exception
   */
  public function testAuthenticationWithNewUserSucceeds() {
    /**
     * @var ConfigFactoryInterface $config_factory
     */
    $config_factory = $this->container->get('config.factory');

    $config = $config_factory->getEditable('basicshib.auth_filter');
    $config->set('create', ['allow' => TRUE, 'error' => '']);
    $config->save();

    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => 'abcd',
            'eppn' => 'jdoe@example.com',
            'mail' => 'jdoe@mail.example.com',
          ]
      );

    $this->container->set('request_stack', $request_stack);

    $handler = $this->container->get('basicshib.authentication_handler');

    try {
      $handler->authenticate();
    }
    catch (AuthenticationException $exception) {
      $this->fail($exception->getTraceAsString());
    }
  }

  /**
   *
   * @throws \Exception
   */
  public function testAuthenticationWithMissingSessionId() {
    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => '',
            'eppn' => 'jdoe@example.com',
            'mail' => 'jdoe@mail.example.com',
          ]
      );

    $this->container->set('request_stack', $request_stack);
    $handler = $this->container->get('basicshib.authentication_handler');
    $handler->authenticate();

    $this->expectExceptionMessage(AuthenticationException::MISSING_ATTRIBUTES);

//    try {
//      $handler->authenticate();
//      $this->fail('Authenticate succeeded, but was not expected to');
//    }
//    catch (AuthenticationException $exception) {
//      self::assertEquals(AuthenticationException::MISSING_ATTRIBUTES, $exception->getCode());
//    }
  }

  /**
   *
   * @throws \Exception
   */
  public function testAuthenticationWithMissingName() {
    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => 'abcd',
            'eppn' => '',
            'mail' => 'jdoe@mail.example.com',
          ]
      );

    $this->container->set('request_stack', $request_stack);
    $handler = $this->container->get('basicshib.authentication_handler');
    $handler->authenticate();

    $this->expectExceptionMessage(AuthenticationException::MISSING_ATTRIBUTES);
  }

  /**
   *
   * @throws \Exception
   */
  public function testAuthenticationWithMissingMail() {
    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => 'abcd',
            'eppn' => 'jdoe@example.com',
            'mail' => '',
          ]
      );

    $this->container->set('request_stack', $request_stack);
    $handler = $this->container->get('basicshib.authentication_handler');
    $handler->authenticate();

    $this->expectExceptionMessage(AuthenticationException::MISSING_ATTRIBUTES);
//      $this->fail('Authenticate succeeded, but was not expected to');
//    }
//    catch (AttributeException $exception) {
//      $exception->getMessage();
//    }
//    catch (AuthenticationException $exception) {
//      self::assertEquals(AuthenticationException::DUPLICATE_ATTRIBUTES, $exception->getCode());
//    }
  }

  /**
   *
   * @throws \Exception
   */
  public function testAuthenticationWithUserBlocked() {
    $user = $this->createUser('jdoe@example.com', 'jdoe@mail.example.com', 0);

    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => 'abcd',
            'eppn' => $user->getAccountName(),
            'mail' => $user->getEmail(),
          ]
      );

    $this->container->set('request_stack', $request_stack);

    $handler = $this->container->get('basicshib.authentication_handler');

    try {
      $handler->authenticate();
      $this->fail('An exception was expected');
    }
    catch (Exception $exception) {
      self::assertEquals(AuthenticationException::USER_BLOCKED, $exception->getCode());
    }
  }

  /**
   * Test clearing session when user is anonymous.
   *
   * @throws \Exception
   * @throws \Exception
   */
  public function testClearSessionWithAnonymousUser() {
    $request = new Request();
    $request->setSession(new Session());
    if ($request->hasSession()) {
      $request->getSession()
        ->set(SessionTracker::VARNAME, '1234');
    }

    $request_stack = new RequestStack();
    $request_stack->push($request);

    $this->container->set('request_stack', $request_stack);
    /** @var AuthenticationHandlerInterface $handler */
    $handler = $this->container->get('basicshib.authentication_handler');

    /** @var AccountProxyInterface $account */
    $account = $this->container->get('current_user');

    if ($request->hasSession()) {
      self::assertTrue($request->getSession()->has(SessionTracker::VARNAME));
    }
    $result = $handler->checkUserSession($request, $account);

    if ($request->hasSession()) {
      self::assertFalse($request->getSession()->has(SessionTracker::VARNAME));
    }
    self::assertEquals(AuthenticationHandlerInterface::AUTHCHECK_LOCAL_SESSION_EXPIRED, $result);
  }

  /**
   * Test clearing session when user is anonymous.
   *
   * @throws EntityStorageException
   * @throws \Exception
   * @throws \Exception
   */
  public function testClearSessionWithAuthUserAndNoSessionVar() {
    $request = new Request();
    $request->setSession(new Session());
    if ($request->hasSession()) {
      $request->getSession()
        ->set(SessionTracker::VARNAME, '1234');
    }

    $request_stack = new RequestStack();
    $request_stack->push($request);

    $this->container->set('request_stack', $request_stack);
    /**
     * @var AuthenticationHandlerInterface $handler
     */
    $handler = $this->container->get('basicshib.authentication_handler');

    /**
     * @var UserInterface $account
     */
    $account = $this->container->get('entity_type.manager')
      ->getStorage('user')
      ->create(['name' => 'test', 'mail' => 'test', 'status' => 1]);
    $account->save();

    user_login_finalize($account);

    $proxy = new AccountProxy(Drupal::service('event_dispatcher'));
    $proxy->setAccount($account);

    self::assertTrue($account->isAuthenticated());
    if ($request->hasSession()) {
      self::assertTrue($request->getSession()->has(SessionTracker::VARNAME));
    }
    $result = $handler->checkUserSession($request, $proxy);

    if ($request->hasSession()) {
      self::assertFalse($request->getSession()->has(SessionTracker::VARNAME));
    }
    self::assertTrue((bool) AuthenticationHandlerInterface::AUTHCHECK_SHIB_SESSION_EXPIRED, $result);
  }

  /**
   * Test clearing session when user is anonymous.
   *
   * @throws EntityStorageException
   * @throws \Exception
   * @throws \Exception
   */
  public function testClearSessionWithAuthUserAndMismatchedSessionVar() {
    $request = new Request();
    $request->setSession(new Session());
    if ($request->hasSession()) {
      $request->getSession()
        ->set(SessionTracker::VARNAME, '1234');
    }

    $request->server->set('Shib-Session-ID', '4321');

    $request_stack = new RequestStack();
    $request_stack->push($request);

    $this->container->set('request_stack', $request_stack);
    /**
     * @var AuthenticationHandlerInterface $handler
     */
    $handler = $this->container->get('basicshib.authentication_handler');

    /**
     * @var UserInterface $account
     */
    $account = $this->container->get('entity_type.manager')
      ->getStorage('user')
      ->create(['name' => 'test', 'mail' => 'test', 'status' => 1]);
    $account->save();

    user_login_finalize($account);

    $proxy = new AccountProxy(Drupal::service('event_dispatcher'));
    $proxy->setAccount($account);

    self::assertTrue($account->isAuthenticated());
    if ($request->hasSession()) {
      self::assertTrue($request->getSession()->has(SessionTracker::VARNAME));
    }
    $result = $handler->checkUserSession($request, $proxy);
    if ($request->hasSession()) {
      self::assertFalse($request->getSession()->has(SessionTracker::VARNAME));
    }
    self::assertTrue((bool) AuthenticationHandlerInterface::AUTHCHECK_SHIB_SESSION_ID_MISMATCH, $result);
  }

  /**
   * Test clearing session when user is anonymous.
   *
   * @throws EntityStorageException
   * @throws \Exception
   * @throws \Exception
   */
  public function testClearSessionWithAuthUserAndSameSessionIdIsIgnored() {
    $request = new Request();
    $request->setSession(new Session());
    if ($request->hasSession()) {
      $request->getSession()
        ->set(SessionTracker::VARNAME, '1234');
    }

    $request->server->set('Shib-Session-ID', '1234');

    $request_stack = new RequestStack();
    $request_stack->push($request);

    $this->container->set('request_stack', $request_stack);
    /**
     * @var AuthenticationHandlerInterface $handler
     */
    $handler = $this->container->get('basicshib.authentication_handler');

    /**
     * @var UserInterface $account
     */
    $account = $this->container->get('entity_type.manager')
      ->getStorage('user')
      ->create(['name' => 'test', 'mail' => 'test', 'status' => 1]);
    $account->save();

    user_login_finalize($account);

    $proxy = new AccountProxy(Drupal::service('event_dispatcher'));
    $proxy->setAccount($account);

    self::assertTrue($account->isAuthenticated());
    if ($request->hasSession()) {
      self::assertTrue($request->getSession()->has(SessionTracker::VARNAME));
    }
    $result = $handler->checkUserSession($request, $proxy);

    if ($request->hasSession()) {
      self::assertTrue($request->getSession()->has(SessionTracker::VARNAME));
    }
    self::assertEquals(AuthenticationHandlerInterface::AUTHCHECK_IGNORE, $result);
  }

  /**
   *
   * @throws \Exception
   */
  public function testGetLoginUrl() {
    $url = $this->getMockBuilder(Url::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['setAbsolute', 'toString'])
      ->getMock();
    $url->method('toString')
      ->willReturn('https://example.com/foo');

    $path_validator = $this->getMockForAbstractClass(PathValidatorInterface::class);
    $path_validator->method('getUrlIfValid')
      ->willReturn($url);
    $this->container->set('path.validator', $path_validator);

    /**
     * @var AuthenticationHandlerInterface $handler
     */
    $handler = $this->container->get('basicshib.authentication_handler');

    $expected_url = Url::fromUserInput('/Shibboleth.sso/Login', ['query' => ['target' => 'https://example.com/foo']]);
    $this->assertEquals($expected_url->toString(), $handler->getLoginUrl());
  }

}
