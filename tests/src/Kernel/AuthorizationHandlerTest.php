<?php

namespace Drupal\Tests\basicshib\Kernel;

use Drupal;
use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\basicshib\AuthorizationHandlerInterface;
use Drupal\basicshib\Exception\AttributeException;
use Drupal\basicshib\Exception\AuthenticationException;
use Drupal\basicshib\SessionTracker;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\basicshib\Traits\MockTrait;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 *
 */
class AuthorizationHandlerTest extends KernelTestBase {
  use MockTrait;

  protected static $modules = ['basicshib', 'user', 'system'];

  /**
   * @var \Drupal\basicshib\SessionTracker
   */
  private SessionTracker $session_tracker;

  /**
   *
   */
  public function setUp(): void{
    parent::setUp();
    $this->installConfig(['basicshib']);
    $this->installEntitySchema('user');
    $this->installSchema('system', 'sequences');
  }

  /**
   * @param $name
   * @param $mail
   * @param int $status
   *
   * @return EntityInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createUser($name, $mail, int $status = 1): EntityInterface {
    /**
     * @var UserStorageInterface $storage
     */
    $storage = $this->container->get('entity_type.manager')
      ->getStorage('user');

    $user = $storage->create(
      [
        'name' => $name,
        'mail' => $mail,
        'status' => $status,
      ]
    );

    $user->save();

    return $user;
  }

  /**
   *
   * @throws \Exception
   */
//  public function testAuthorizationAnonymous() {
//
//    $user = $this->createUser('jdoe@example.com', 'jdoe@mail.example.com');
//    $user->addRole('administrator');
//    $user->save();
//    $request = Request::create('/user/' . $user->getOriginalId(), 'GET');
//    $session = new \Symfony\Component\HttpFoundation\Session\Session();
//    $request->setSession($session);
//
//    /** @var \Symfony\Component\HttpKernel\HttpKernelInterface $kernel */
//    $kernel = $this->container->get('http_kernel');
//    $response = $kernel->handle($request);
//
//    $session = $request->getSession();
//    $session->set('ismemberof', 'hello;yellow');
//    $session->save();
////    $this->assertEquals(TRUE, $session->get('some_session_attribute'), "The expected session attribute is set.");
//    $grouperConfig = $this->container
//      ->get('config.factory')
//      ->get('basicshib.grouper_settings');
//
//    $handler = $this->container->get('basicshib.authorization_handler');
//    $handler->authorize($user);
// }
//    $request_stack = $this->getMockRequestStack(
//      [
//        'Shib-Session-ID' => 'abcd',
//        'eppn' => $user->getAccountName(),
//        'mail' => $user->getEmail(),
//      ]
//    );
//
//    $this->container->set('request_stack', $request_stack);
//
//    $handler = $this->container->get('basicshib.authorization_handler');
//
//    try {
//      $handler->authorize();
//    }
//    catch (Exception $exception) {
//      $this->fail(sprintf('An exception was thrown: %s', $exception->getTraceAsString()));
//    }
//  }

  // Set attributes

  // Check attributes anonymous
  // Check attributes authenticated
  // Check attributes administrator
  // Remove roles all
  // Remove administrator role
  // Add editor role

}
