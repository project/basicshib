<?php

namespace Drupal\Tests\basicshib\Kernel;

use Drupal;
use Drupal\basicshib\Plugin\basicshib\grouper\GrouperPluginDefault;
use Drupal\Core\Config\Config;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\basicshib\Traits\MockTrait;
use Drupal\user\Entity\Role;

/**
 *
 */
class GrouperPluginImplementationTest extends KernelTestBase {
  use MockTrait;

  protected static $modules = [
    'basicshib',
//    'basicshib_test',
    'user',
    'system',
  ];

  /**
   * Setup function for the test.
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['basicshib', 'user']);
    $this->installEntitySchema('user');
    $this->installSchema('system', 'sequences');

    $entity_type_manager = $this->container
      ->get('entity_type.manager');
    $this->userStorage = $entity_type_manager
      ->getStorage('user');

    // Insert a row for the anonymous user.
    $this->userStorage
      ->create([
        'uid' => 0,
        'name' => '',
        'status' => 0,
      ])
      ->save();

    // Create user 1 so that the user created later in the test has a different
    // user ID.
    // @todo Remove in https://www.drupal.org/node/540008.
    $this->userStorage
      ->create([
        'uid' => 1,
        'name' => 'user1',
      ])
      ->save();

    $roles = [
      ['id' => 'administrator', 'label' => 'Administrator'],
      ['id' => 'editor', 'label' => 'Editor'],
      ['id' => 'contributor', 'label' => 'Contributor'],
    ];
    foreach ($roles as $role) {
      $current_role = Role::create($role);
      $current_role->save();
    }

//    $grouper_roles = [
//      'role_0' => 'hello',
//      'role_1' => 'yellow',
//      'role_2' => 'bellow',
//      'role_3' => 'hello',
//      'role_4' => 'yellow',
//      'role_5' => 'bellow',
//      'role_6' => 'hello',
//      'role_7' => 'yellow',
//      'role_8' => 'bellow',
//    ];

//    $grouperConfig = $this->container->get('config.factory')
//      ->getEditable('basicshib.settings');

  }

  /**
   * Set the configuration.
   */
  private function setShibConfig(bool $setting) {
    /**
     * @var Config $config
     */
    $config = $this->container->get('config.factory')
      ->getEditable('basicshib.settings');

    $config->set('plugin_enabled', ['grouper_enabled' => $setting]);
    $config->save();

//    return $this->container->get('config.factory')
//      ->getEditable('basicshib_test.settings');
  }

  /**
   * Set the configuration.
   */
  private function setGrouperConfig($settings) {
    /**
     * @var Config $config
     */
    $configGrouper = $this->container->get('config.factory')
      ->getEditable('basicshib_grouper_test.settings');

    $config->set('grouper', $settings['roles']);

    $config->save();

    return $this->container->get('config.factory')
      ->getEditable('basicshib_grouper_test.settings');
  }

  public function testGrouperNotEnabled() {

    $this->setShibConfig (FALSE);
    $config = $this->container
      ->get('config.factory')
      ->get('basicshib.settings');

    $enabled = $config->get('plugin_enabled');
    self::assertFalse($enabled['grouper_enabled']);
  }

  public function testGrouperEnabled() {

    $this->setShibConfig (TRUE);
    $config = $this->container
      ->get('config.factory')
      ->get('basicshib.settings');

    $enabled = $config->get('plugin_enabled');
    self::assertTrue($enabled['grouper_enabled']);
  }

  public function testGetMapRoleCount () {

    $this->testGrouperEnabled ();

    $grouperConfig = $this->container
      ->get('config.factory')
      ->get('basicshib.settings');

    $grouper = new GrouperPluginDefault($grouperConfig);
    $map = $grouper->getMap();
    self::assertCount(3, $map);
  }

  public function testGetMapEditorRoleExists () {

    $this->testGrouperEnabled ();

    $grouperConfig = $this->container
      ->get('config.factory')
      ->get('basicshib.grouper_settings');

    $grouper = new GrouperPluginDefault($grouperConfig);
    $map = $grouper->getMap();
    self::assertContains('editor', $map, 'Editor role not found.');
  }

  public function testGetMapAuthenticatedRoleNotExists () {

    $this->testGrouperEnabled ();

    $grouperConfig = $this->container
      ->get('config.factory')
      ->get('basicshib.grouper_settings');

    $grouper = new GrouperPluginDefault($grouperConfig);
    $map = $grouper->getMap();
    self::assertNotContains('authenticated', $map, 'Authenticated role found.');
  }
}
