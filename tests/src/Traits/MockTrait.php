<?php

namespace Drupal\Tests\basicshib\Traits;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 *
 */
trait MockTrait {

  /**
   * @param array $settings
   * @return \PHPUnit_Framework_MockObject_MockObject|ConfigFactoryInterface
   */
  public function getMockConfigFactory(array $settings = []) {
    $settings += [
      'basicshib.settings' => [],
      'basicshib.grouper_settings' => [],
      'basicshib.auth_filter' => [],
    ];

    $settings['basicshib.settings'] += [
      'attribute_map' => [
        'key' => [
          'session_id' => 'Shib-Session-ID',
          'name' => 'eppn',
          'mail' => 'mail',
        ],
        'optional' => [
            ['id' => 'opt1', 'name' => 'OPT1'],
        ],
      ],
      'handlers' => [
        'login' => '/Shibboleth.sso/Login',
        'logout' => '/Shibboleth.sso/Logout',
      ],
      'authentication_plugin' => 'basicshib',
      'authorization_plugin' => 'basicshib_grouper',
      'generic_login_error_message' => 'generic_login_error',
      'account_blocked_message' => 'user_blocked',
      'default_post_login_redirect_path' => '/user',
    ];

    $settings['basicshib.auth_filter'] += [
      'create' => [
        'allow' => 'false',
        'error' => 'user_creation_not_allowed',
      ],
    ];

    $settings['basicshib.grouper_settings'] += [
      'role_1' => 'yellow',
      'role_2' => 'bellow',
       'role_3' => 'hello',
       'role_4' => 'yellow',
       'role_5' => 'bellow',
       'role_6' => 'hello',
       'role_7' => 'yellow',
       'role_8' => 'bellow'
    ];

    $config_factory_map = [];
    foreach (array_keys($settings) as $config_source) {
      $config = $this->getMockBuilder(ImmutableConfig::class)
        ->disableOriginalConstructor()
        ->onlyMethods(['get'])
        ->getMock();

      $config_map = [];

      foreach ($settings[$config_source] as $config_name => $config_value) {
        $config_map[] = [$config_name, $config_value];
      }

      $config->method('get')
        ->willReturnMap($config_map);

      $config_factory_map[] = [$config_source, $config];
    }

    $config_factory = $this->getMockForAbstractClass(ConfigFactoryInterface::class);
    $config_factory->method('get')
      ->willReturnMap($config_factory_map);

    return $config_factory;
  }

  /**
   * @param array $settings
   * @return \PHPUnit_Framework_MockObject_MockObject|ConfigFactoryInterface
   */
  public function getMockGroups() {
    $groups = [
      'basicshib.grouper.settings' => [
        'role_0' => 'hello',
        'role_1' => 'yellow',
        'role_2' =>'bellow',
        'role_3' =>'hello',
        'role_4' =>'yellow',
        'role_5' =>'bellow',
        'role_6' =>'hello',
        'role_7' => 'yellow',
        'role_8' => 'bellow'
      ]
    ];

    return $groups;
  }

//  /**
//   * @param array $roles
//   * @return \PHPUnit_Framework_MockObject_MockObject
//   */
//  public function getMockRoles() {
//    $roles = $this->getMockForAbstractClass(User::class);
//      $roles->method('getRoles')
//        ->willReturn($roles['roles']);
//
//    return $roles;
//  }

  /**
   *
   */
  public function getMockUser(array $properties) {
    $user = $this->getMockForAbstractClass(UserInterface::class);

    if (isset($properties['name'])) {
      $user->method('getAccountName')
        ->willReturn($properties['name']);
    }

    if (isset($properties['mail'])) {
      $user->method('getEmail')
        ->willReturn($properties['mail']);
    }

    $user->method('isBlocked')
      ->willReturn(empty($properties['status']));

    if (isset($properties['roles'])) {
      $user->method('getRoles')
        ->willReturn($properties['roles']);
    }

    return $user;
  }

  /**
   * @param UserInterface[] $existing_users
   *
   * @return UserStorageInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  public function getMockUserStorage(array $existing_users = []) {
    $existing_map = [];

    foreach ($existing_users as $user) {
      $existing_map[$user->getAccountName()] = $user;
    }

    $storage = $this->getMockForAbstractClass(UserStorageInterface::class);
    $storage->method('loadByProperties')
      ->willReturnCallback(
              function (array $properties) use ($existing_map) {
                if (isset($existing_map[$properties['name']])) {
                    return [$existing_map[$properties['name']]];
                }
                  return [];
              }
          );
    $storage->method('create')
      ->willReturnCallback(
              function (array $properties) {
                  return $this->getMockUser($properties);
              }
          );
    return $storage;
  }

  /**
   * @param UserInterface[] $existing_users
   *
   * @return UserStorageInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  public function getMockGrouper(array $existing_users = []) {
    $existing_map = [];

    foreach ($existing_users as $user) {
      $existing_map[$user->getAccountName()] = $user;
    }

    $storage = $this->getMockForAbstractClass(UserStorageInterface::class);
    $storage->method('loadByProperties')
      ->willReturnCallback(
        function (array $properties) use ($existing_map) {
          if (isset($existing_map[$properties['name']])) {
            return [$existing_map[$properties['name']]];
          }
          return [];
        }
      );
    $storage->method('create')
      ->willReturnCallback(
        function (array $properties) {
          return $this->getMockUser($properties);
        }
      );
    return $storage;
  }

  /**
   * @param array $values
   * @return \PHPUnit\Framework\MockObject\MockObject|SessionInterface
   */
  public function getMockSession(array $values = []) {
    $session = $this->getMockForAbstractClass(SessionInterface::class);
    $session->method('get')
      ->willReturnCallback(
              function ($name, $default = NULL) use ($values) {
                  return $values[$name] ?? $default;
              }
          );
    return $session;
  }

  /**
   *
   */
  public function getMockRequestStack(array $server = [], $session = NULL): RequestStack {
    $request_stack = new RequestStack();
    $request = new Request([], [], [], [], [], $server);
    if ($session === NULL) {
      $request->setSession($this->getMockSession());
    }
    $request_stack->push($request);
    return $request_stack;
  }

  public function getMockRoles($exclude_locked_roles = TRUE) {
    $roles = [
      'administrator',
      'content_creator',
      'content_editor',
      'reviewer'
    ];

    // Users with an ID always have the authenticated user role.
    if (!$exclude_locked_roles) {
      if ($this
        ->isAuthenticated()) {
        $roles[] = 'authenticated';
      }
      else {
        $roles[] = 'anonymous';
      }
    }

    return $roles;
  }
}
