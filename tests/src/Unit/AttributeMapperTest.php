<?php

namespace Drupal\Tests\Unit\basicshib;

use Drupal\basicshib\AttributeMapper;
use Drupal\basicshib\Exception\AttributeException;
use Drupal\Tests\basicshib\Traits\MockTrait;
use Drupal\Tests\UnitTestCase;

/**
 *
 */
class AttributeMapperTest extends UnitTestCase {
  use MockTrait;

  /**
   * Test getting an attribute under valid circumstances.
   *
   * @throws AttributeException
   */
  public function testGetAttribute() {
    $value = $this->randomMachineName();

    $config_factory = $this->getMockConfigFactory();
    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => $value,
          ]
      );

    $mapper = new AttributeMapper($config_factory, $request_stack);

    $this->assertEquals($value, $mapper->getAttribute('session_id'));
  }

  /**
   * Test attempting to get an unmapped attribute.
   *
   * @throws AttributeException
   */
  public function testGetAttributeUnmapped() {
    $config_factory = $this->getMockConfigFactory(
          [
            'basicshib.settings' => ['attribute_map' => ['key' => [], 'optional' => []]],
          ]
      );

    $request_stack = $this->getMockRequestStack();

    $mapper = new AttributeMapper($config_factory, $request_stack);

    try {
      $mapper->getAttribute('session_id');
    }
    catch (AttributeException $exception) {
      $this->assertEquals(AttributeException::NOT_MAPPED, $exception->getCode());
    }

    $this->assertNotFalse(isset($exception));
  }

  /**
   * Test getting an attribute when unset and empty is not allowed.
   *
   * @throws AttributeException
   */
  public function testGetAttributeUnsetAndEmptyNotAllowed() {
    $config_factory = $this->getMockConfigFactory();

    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => '',
          ]
      );

    $mapper = new AttributeMapper($config_factory, $request_stack);

    try {
      $mapper->getAttribute('session_id');
    }
    catch (AttributeException $exception) {
      $this->assertEquals(AttributeException::NOT_SET, $exception->getCode());
    }

    $this->assertNotFalse(isset($exception));
  }

  /**
   * Test getting an attribute when unset and empty is not allowed.
   *
   * @throws AttributeException
   */
  public function testGetAttributeUnsetAndEmptyAllowed() {
    $config_factory = $this->getMockConfigFactory();

    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => '',
          ]
      );

    $mapper = new AttributeMapper($config_factory, $request_stack);

    try {
      $mapper->getAttribute('session_id', TRUE);
    }
    catch (AttributeException $exception) {
    }

    $this->assertFalse(isset($exception));
  }

  /**
   *
   * @throws AttributeException
   */
  public function testGetOptionalAttribute() {
    $config_factory = $this->getMockConfigFactory();

    $request_stack = $this->getMockRequestStack(
          [
            'OPT1' => 'ok',
          ]
      );

    $mapper = new AttributeMapper($config_factory, $request_stack);

    $this->assertEquals('ok', $mapper->getAttribute('opt1', TRUE));
  }

  /**
   * Assert that an optional attribute that duplicates another attribute throws
   * an exception.
   */
  public function testDuplicatingAttributeThrowsException() {
    $config_factory = $this->getMockConfigFactory(
          [
            'basicshib.settings' => [
              'attribute_map' => [
                'key' => [
                  'session_id' => 'Shib-Session-ID',
                ],
                'optional' => [
                ['id' => 'session_id', 'name' => 'other'],
                ],
              ],
            ],
          ]
      );

    $request_stack = $this->getMockRequestStack(
          [
            'Shib-Session-ID' => '',
          ]
      );

    try {
      new AttributeMapper($config_factory, $request_stack);
      $this->fail('An exception was expected');
    }
    catch (AttributeException $exception) {
      $this->assertEquals(AttributeException::DUPLICATE_ID, $exception->getCode());
    }
  }

}
