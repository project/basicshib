<?php

namespace Drupal\Tests\basicshib\Unit\Plugin;

use Drupal\basicshib\Plugin\basicshib\user_provider\UserProviderPluginDefault;
use Drupal\Tests\basicshib\Traits\MockTrait;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;

/**
 *
 */
class UserProviderPluginDefaultTest extends UnitTestCase {
  use MockTrait;

  /**
   *
   */
  public function testLoadUser() {
    $mockUser = $this->getMockUser(['name' => 'jdoe']);
    $provider = new UserProviderPluginDefault($this->getMockUserStorage([$mockUser]));
    $user = $provider->loadUserByName('jdoe');
    $this->assertTrue(is_a($user, UserInterface::class));
    $this->assertEquals('jdoe', $user->getAccountName());

    $user = $provider->loadUserByName('-');
    $this->assertNull($user);
  }

  /**
   *
   */
  public function testCreateUser() {
    $provider = new UserProviderPluginDefault($this->getMockUserStorage());
    $user = $provider->createUser('jdoe', 'jdoe@example.com');
    $this->assertNotNull($user);
    $this->assertEquals('jdoe', $user->getAccountName());
    $this->assertEquals('jdoe@example.com', $user->getEmail());
  }

}
